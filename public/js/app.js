"use strict";
var app = angular.module("videoApp", [
    "ngRoute",
    "ngResource",
    "ngMessages",
    "ngSanitize",
    "videoApp.controllers",
    "videoApp.services",
    "videoApp.directives",
    "ngMaterial",
    "ngAnimate",
    "ngAria"
]);


app.config(["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {
    $routeProvider

        /*
         *  contains whats new
         */
        .when("/", {
            templateUrl:    "public/pages/index.html",
            controller:     "indexController",
            requireLogin:   false
        })

        .when("/index", {
            redirectTo: "/"
        })

        /*
         *  Search specific
         */
        // Get a list of videos \ users \ playlists that matches :query
        .when("/search/:query", {
            templateUrl:    "public/pages/search.html",
            controller:     "searchController",
            requireLogin:   false
        })

        /**
         * Videos related
         */
        // video specific
        .when("/watch/:video_id/:time?", {
            templateUrl:    "public/pages/watch.html",
            controller:     "watchController",
            requireLogin:   false
        })
        // latest videos
        .when("/watch", {
            redirectTo: "/"
        })

        /**
         * Authentication
         */
        // login site for members
        .when("/signin", {
            templateUrl:    "public/pages/signin.html",
            controller:     "signinController",
            requireLogin:   true
        })

        // sign out
        .when("/signout", {
            template: '',
            controller:     "signoutController",
            requireLogin:   true
        })

        /**
         * Dashboard
         */
        .when("/dashboard/:tab?/:id?/:action?", {
            templateUrl:    "public/pages/dashboard.html",
            controller:     "dashboard-dashboardController",
            requireLogin:   true
        })

        /**
         * Other
         */
        .when("/videolog", {
            templateUrl:    "public/pages/videolog.html",
            controller:     "videologController",
            requireLogin:   false
        })


        /**
         * Default route
         */
        .otherwise({
            templateUrl: "public/pages/404.html",
            requireLogin:   false
        });

    $locationProvider.html5Mode(true);
}]);

app.constant('USER_ROLES', [
    "guest",    //0
    "member",   //1
    "admin"     //2
]);



app.run(["$rootScope", "$location", "$route", "Session", "$http", "Users", function ($rootScope, $location, $route, Session, $http, Users) {
    /**
     * When a route starts, get the user data and store it
     */
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        Users.session(function (user) {
            Session.update(user); // store user

            /**
             * if a access config is set, validate it against the user data.
             */
            if (Session.authenticated() !== next.requireLogin && next.requireLogin) {
                $location.path("/signin");
            }
        });

    });

    /**
     * update the url without relaod the controller or path!
     * source: http://joelsaupe.com/programming/angularjs-change-path-without-reloading/
     *
     * usage: $location.path("/dashboard/my-videos", false); //doesn't update controller!
     */
    var original = $location.path;
    $location.path = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };
}]);