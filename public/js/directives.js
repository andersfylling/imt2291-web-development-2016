"use strict";
var app = angular.module("videoApp.directives", []);

/**
 * initiates the videoplayer, all that is needed is a video id!
 */
app.directive("videoplayer", ["$routeParams", "$window", "Videos", function ($routeParams, $window, Videos) {

    /**
     * Videoplayer functions
     */
    function link(scope, element, attrs) {
        scope.mainProgress.wait();//start the progress bar

        /**
         * vars
         */
        var e           = element[0];

        var vp          = e.querySelector("#videoplayer");
        var playPause   = e.querySelector("#videoplayer-playPause");
        var mute        = e.querySelector("#videoplayer-mute");
        var bar         = e.querySelector("#videoplayerProgress");

        var blackboard  = e.querySelector("#blackboard");


        /*
         * Videoplayer (#videoplayer)
         */
        scope.videoplayer = {
            /**
             * Videoplayer values
             */
            src:        "api/videos/stream/" + $routeParams.video_id,
            extension:  "mp4",
            playtime:   0,
            duration:   1,
            playable:   false,
            running:    0, //0, 1, 2: paused, playing, replay
            volume:     100,
            self:       vp,
            ct:         parseFloat($routeParams.time) || 0,
            muted:      false,

            /**
             * Videoplayer object functions
             */
            loadedmetadata: function (e) {
                /**
                 * If time has been specified make sure the video starts
                 *  at the given time. Always in seconds.
                 */
                if (vp.currentTime != scope.videoplayer.ct) {
                    vp.currentTime = scope.videoplayer.ct;
                }

                /**
                 * set scope values
                 */
                scope.videoplayer.duration  = vp.duration;

                /**
                 * Adds tracks
                 */
                addTracks();

                /**
                 * Tell angular at there has been an update
                 */
                scope.$apply();
            },
            canplay: function (e) {

                /**
                 * set scope values
                 */
                scope.videoplayer.playable  = true;
                scope.mainProgress.finished(true);

                /**
                 * Tell angular at there has been an update
                 */
                scope.$apply();
            },
            play: function (e) {
                if (vp.paused || vp.ended) {
                    /**
                     * If the videoplayer is paused or done and
                     * a play request was done, change the icon to play
                     * and play the media from the current range or replay it.
                     */
                    scope.videoplayer.running = 1;
                    vp.play();
                } else {
                    /**
                     * If the videoplayer is paused or done and
                     * a play request was done, pause the video, and change
                     * the icon to pause.
                     */
                    scope.videoplayer.running = 0;
                    vp.pause();
                }
            },
            mute: function () {
                if (vp.muted) {
                    scope.videoplayer.muted = false;
                    scope.videoplayer.volume = 100;
                    vp.muted = false;
                } else {
                    scope.videoplayer.muted = true;
                    scope.videoplayer.volume = 0;
                    vp.muted = true;
                }
            },
            changeVolume: function () {
                if (scope.videoplayer.volume == 0) {
                    scope.videoplayer.mute(); //mute
                } else {
                    vp.muted  = false;
                    scope.videoplayer.muted = false;
                    vp.volume = scope.videoplayer.volume / 100;
                }
            },
            ended: function () {
                scope.videoplayer.running = 2;
                scope.$apply(); //update scope
            },
            timeupdate: function (e) {
                scope.videoplayer.playtime = vp.currentTime * 100 / scope.videoplayer.duration;
                scope.$apply();
            },

            /**
             * Videoplayer controllers
             */
            controllers: {
                setBar: function (e) {
                    /**
                     * If the video can be played, selecting a new time on
                     * the progress bar will change the current time in the
                     * video player.
                     */
                    if (scope.videoplayer.playable) {
                        vp.currentTime = vp.duration * (e.offsetX / bar.offsetWidth);
                    }
                },
                setSpeed: function (speed) {
                    vp.playbackRate = speed;
                }
            }
        };


        /**
         * Video buffering prevents the page from reloading.
         * To stop the buffer the video must start and then be stopped.
         * However this plays the first bytes of sounds, which isn't pleasant.
         *
         * videoplayer.mute() does not work, and .volume must be used.
         */
        angular.element($window).on('beforeunload', function (event) {
            vp.volume = 0;
            vp.play();
            vp.pause();
        });

        /**
         * Event listeners: Videoplayer
         */
        vp.addEventListener("loadedmetadata",   scope.videoplayer.loadedmetadata);
        vp.addEventListener("canplay",          scope.videoplayer.canplay);
        vp.addEventListener("click",            scope.videoplayer.play);
        vp.addEventListener("timeupdate",       scope.videoplayer.timeupdate);
        vp.addEventListener("ended",            scope.videoplayer.ended);

        /**
         * Event listeners: Controller
         */
        playPause.addEventListener("click",     scope.videoplayer.play);
        mute.addEventListener("click",          scope.videoplayer.mute);

        /**
         * Event listeners: Progress bar
         */
        bar.addEventListener("click",           scope.videoplayer.controllers.setBar);

        /**
         * Add text tracks!
         */
        function addTracks() {
            Videos.getTracks({id: $routeParams.video_id}, function (tracks) {
                tracks.map(function (track, i){
                    var t = document.createElement("track");
                    t.kind = "metadata";
                    t.label = "English";
                    t.srclang = "en";
                    t.src = "public/vtt/137.vtt";
                    vp.appendChild(t);

                });
                scope.tracks = tracks;
            });
            scope.activeStartTime = 0;

            scope.useTrack = function (i) {
                vp.textTracks[i].mode = "showing";

                vp.textTracks[i].oncuechange = function () {
                    console.log(this.activeCues[0]);
                    scope.activeStartTime = this.activeCues[0].startTime;
                };
            };
        }

    }

    /**
     * Return the video player instance
     */
    return {
        templateUrl: "public/templates/videoplayer.html",
        replace: true,
        link: link
    };
}]);


app.directive("contentProgressbar", [function () {

    var process = {
        wait: false,
        finished: true
    };

    function link(scope, element, attrs) {
        scope.mainProgressMode = "determinate";
        scope.mainProgressValue = 0;

        scope.mainProgress = {
            finished: function (ready) {
                ready = typeof ready !== 'undefined' ? ready : false;

                if (process.finished) {
                    return;
                }

                if (process.wait && ready === false) {
                    return;
                }

                scope.mainProgressMode = "determinate";
                scope.mainProgressValue = 100;
                process.wait = false;
                process.finished = true;

                scope.$apply();
            },
            loading: function () {
                process.wait = false;
                process.finished = false;
                scope.mainProgressMode = "indeterminate";
            },
            wait: function () {
                process.wait = true;
            }
        };

        function stop () {
            setTimeout(function () {
                scope.mainProgress.finished();
            }, 1000);
        }

        scope.$on('$locationChangeStart', function (event) {
            scope.mainProgress.loading();
        });
        scope.$on('$viewContentLoaded', stop);
        scope.$on('$locationChangeSuccess', stop);
    }

    return link;
}]);


app.directive("videoContextMenu", ["$mdDialog", function ($mdDialog) {

    function link(scope, element, attrs) {
        scope.al = function() {
            console.log("asdfasd");
        };

        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();

                $mdDialog.show({
                    templateUrl: "public/templates/videoContextMenu.html",
                    clickOutsideToClose: true
                });
            });
        });
    }

    return {
        //templateUrl: "public/templates/videoContextMenu.html",
        link: link
    };
}]);

app.directive('isAuthenticated', ["Session", function(Session) {
    return function(scope, element, attr) {
        return Session.authenticated();
    };
}]);