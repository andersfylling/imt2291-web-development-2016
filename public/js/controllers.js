"use strict";
var app = angular.module("videoApp.controllers", []);


app.controller("indexController", ["$scope", "Videos", "Playlists", function ($scope, Videos, Playlists) {
    $scope.videos       = Videos.query();
    $scope.playlists    = Playlists.query();
}]);

app.controller("searchController", [function () {
    console.log("searching");
}]);

app.controller("signinController", ["$rootScope", "$scope", "Users", "Session", "$location", function ($rootScope, $scope, Users, Session, $location) {
    $scope.form = {
        email:      "",
        password:   ""
    };

    $scope.alerts = [];
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.login = function () {
        if ($scope.form.email.length < 5 || $scope.form.password < 5) {
            $scope.alerts.push({
                type: "danger",
                msg: 'Email or password was less than 5 characters long!'
            });
            return;
        }

        /**
         * store the user object
         */
        Users.login($scope.form, function (user) {
            Session.create(user);
            $location.path("/dashboard");
        });
    }
}]);

app.controller("signoutController", ["Session", "Users", "$location", function (Session, Users, $location) {
    Users.signout(function () {
        console.log(1);
        Session.destroy();

        $location.path("/signin");
    });
}]);

app.controller("watchController", ["$scope", "$routeParams", "Videos", "Playlists", "$mdToast", function ($scope, $routeParams, Videos, Playlists, $mdToast) {

    $scope.video = Videos.query({id: $routeParams.video_id}, function (video) {

        /**
         * save video to local storage
         */
        var videologs = JSON.parse(localStorage.getItem("videologs"));
        if (videologs == null) {
            videologs = [];
        }
        videologs.push({time: new Date(), video: video[0]});

        localStorage.setItem("videologs", JSON.stringify(videologs));
    })[0];

    Playlists.getListOfUser({id: $scope.session.id}, function (playlists) {
        $scope.myPlaylists = playlists;
        console.log($scope.myPlaylists);
    });

    $scope.addVideoToPlaylist = function (playlist_id) {
        Playlists.addVideo({video_id: $routeParams.video_id, playlist_id: playlist_id}, function (playlists) {
            if (playlists.length > 0) {
                $mdToast.showSimple("Updated your profile!");
            }
        });
    };


}]);

/**
 * Retrieves videos from local storage
 */
app.controller("videologController", ["$scope", "$mdToast", function ($scope, $mdToast) {
    var videologs = JSON.parse(localStorage.getItem("videologs"));
    if (videologs == null) {
        videologs = [];
    }

    $scope.videos = videologs;

    var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };
    $scope.toastPosition = angular.extend({},last);
    $scope.getToastPosition = function() {
        sanitizePosition();
        return Object.keys($scope.toastPosition)
            .filter(function(pos) { return $scope.toastPosition[pos]; })
            .join(' ');
    };
    function sanitizePosition() {
        var current = $scope.toastPosition;
        if ( current.bottom && last.top ) current.top = false;
        if ( current.top && last.bottom ) current.bottom = false;
        if ( current.right && last.left ) current.left = false;
        if ( current.left && last.right ) current.right = false;
        last = angular.extend({},current);
    }

    $scope.removeEntry = function(index) {
        $scope.videos.splice($scope.videos.length - 1 - index, 1);
        localStorage.setItem("videologs", JSON.stringify($scope.videos));
        $mdToast.show(
            $mdToast.simple()
                .textContent('Entry deleted!')
                .position($scope.getToastPosition())
                .hideDelay(2000)
        );
    };

}]);


/**
 * Dashboard
 * CONTROLLER-dashboardController
 *
 *
 */
app.controller("dashboard-dashboardController", ["$scope", "$routeParams", "$location", function ($scope, $routeParams, $location){
    /**
     * Private
     */
    var tabs = [
        "profile",
        "my-videos",
        "my-playlists",
        "management"
    ];

    /**
     * scope
     */
    $scope.selectedTabIndex = 0;

    /**
     * scope functions
     */
    $scope.changeDashboardSection = function () {
        $location.path("/dashboard/" + tabs[$scope.selectedTabIndex], false);
    };

    //fallback..
    $scope.save = function () {
        console.log(1);
    };


    /**
     * Potential changes
     */
    if (tabs.indexOf($routeParams.tab) >= 0) {
        $scope.selectedTabIndex = tabs.indexOf($routeParams.tab);
    }
}]);

/**
 * Profile
 * CONTROLLER-dashboardController
 */
app.controller("profile-dashboardController", ["$scope", "Users", "Session", "$mdToast", function ($scope, Users, Session, $mdToast){
    $scope.profile = $scope.session;

    /**
     * scope functions
     */
    $scope.save = function () {
        Users.update({id: $scope.profile.id}, $scope.profile, function (user) {
            Session.update(user);
            $mdToast.showSimple("Updated your profile!");
        });
    };

    /**
     * scope updaters
     */
}]);

/**
 * Videos
 * CONTROLLER-dashboardController
 */
app.controller("videos-dashboardController", ["$scope", "Videos", function ($scope, Videos) {

}]);

/**
 * Playlists
 * CONTROLLER-dashboardController
 */
app.controller("playlists-dashboardController", ["$scope", "Playlists", "$mdToast", function ($scope, Playlists, $mdToast) {
    $scope.playlists = [];
    $scope.newPlaylist = {
        title: "",
        description: "",
        user_id: $scope.session.id
    };

    Playlists.getListOfUser({id: $scope.session.id}, function (playlists) {
        $scope.playlists = playlists;
        playlists.map(function (list, index) {
            Playlists.getVideos({id: list.id}, function (videos) {
                $scope.playlists[index].videos = videos;
            });
        });


    });

    /**
     * Register a new playlist
     */
    $scope.register = function () {
        Playlists.register($scope.newPlaylist, function (playlist) {
            if (playlist.id >= 0) {
                var list = $scope.newPlaylist;
                list.videos = [];
                $scope.playlists.push(list);
                $mdToast.showSimple("*" + $scope.newPlaylist.title + "* have been registered!");
            }
        });
    };
}]);

/**
 * Users
 * CONTROLLER-dashboardController
 */
app.controller("users-dashboardController", ["$scope", "Users", "$mdToast", "$mdDialog", "$mdMedia", function ($scope, Users, $mdToast, $mdDialog, $mdMedia) {

    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
    $scope.users = Users.query();
    $scope.editUser = {
        id: 0,
        name: "",
        email: "",
        password: "",
        level: 1
    };
    $scope.newUser = {
        name: "",
        email: "",
        password: "",
        level: 1
    };


    /**
     * Register a new user. must be by an admin
     */
    $scope.register = function () {
        Users.register($scope.newUser, function (user) {
            if (user.id >= 0) {
                $mdToast.showSimple("*" + $scope.newUser.name + "* have been registered!");
            }
        });
    };

    /**
     * Show info about a user
     */
    $scope.showUserDialog = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
        $mdDialog.show({
                templateUrl: 'public/templates/dashboard-users-about.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen
            })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function() {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    /**
     * edit a user
     */
    $scope.showUserEditDialog = function (ev, index) {
        $scope.editUser = JSON.parse( JSON.stringify($scope.users[index]) );

        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
        $mdDialog.show({
                templateUrl: 'public/templates/dashboard-users-about.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen
            })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function() {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };
    $scope.update = function () {
        Users.update({id: $scope.editUser.id}, $scope.editUser, function (user) {
            if (user.id >= 0) {
                $mdToast.showSimple("*" + $scope.newUser.name + "* have been registered!");
            }
        });
    };

}]);


/**
 * Public controller for every element inside the body tag!
 * TODO: single controllers for individual menus..
 */
app.controller('AppCtrl', [
    '$scope',
    "$rootScope",
    "Session",
    '$mdSidenav',
    "$location",
    "$mdDialog",
    "Videos",
    "Playlists",
    "USER_ROLES",

    function(
        $scope,
        $rootScope,
        Session,
        $mdSidenav,
        $location,      // For redirecting
        $mdDialog,
        Videos,         // Calling videos api methods
        Playlists,       // Calling playlist api methods
        USER_ROLES
    ){

    /**
     * Global variables
     */
    $scope.isAuthenticated  = Session.authenticated(); //Default
    $scope.session          = Session.content();
    $scope.user_levels = USER_ROLES;


    /**
     * Global scope functions
     */
    $scope.goto = function ($url) {
        $location.path($url);
    };

        /**
         * Global scope objects
         * TODO: put into personal controller, no need to be global.
         * except toggle menu
         */
    $scope.toolbar = {
        title: project.name,
        search: {
            visible: false,
            input: ""
        },
        menu: {
            toggle: function($mdOpenMenu, ev) {
                $mdOpenMenu(ev);
            }
        }
    };

    $scope.sidenav = {
        toggle: function (menuId) {
            $mdSidenav(menuId).toggle();
        },
        list: {
            default: [
                {title:"Home",      icon: "home", href: "/"},
                {title:"Videolog",  icon: "list", href: "/videolog"}
            ],
            playlists: Playlists.query(),
            videos: Videos.query(),
            strLimit: 16
        }
    };


    /**
     * Update glocal scope variables
     */
    $rootScope.$on("sessionChange", function () {
        $scope.isAuthenticated  = Session.authenticated();
        $scope.session          = Session.content();
    });

}]);