"use strict";
var services = angular.module("videoApp.services", []);




/**
 * API handling
 */
app.factory('Users', ["$resource", function ($resource) {
    var obj = $resource("api/users/:id", {id: "@_id"}, {
        login:      {method: "POST",    url: "api/users/login"},
        signout:    {method: "HEAD",    url: "api/users/logout"},
        profile:    {method: "GET",     url: "api/users/profile", isArray: false},
        session:    {method: "GET",     url: "api/users/session", isArray: false},
        update:     {method: "PUT"},
        register:   {method: "POST",    url: "api/users/register"}
    });

    return obj;
}]);

app.factory('Videos', ["$resource", function ($resource) {
    var obj = $resource("api/videos/:id", {id: "@_id"}, {
        getTracks:  {method: "GET", url: "api/video/:video_id/tracks", params: {video_id: "@_id"}}
    });

    return obj;
}]);

app.factory('Playlists', ["$resource", function ($resource) {
    var obj = $resource("api/playlists/:id", {id: "@_id"}, {
        getListOfUser:    {method: "GET", url: "api/playlists/users/:user_id", params: {user_id: "@_id"}, isArray: true},
        getVideos:      {method: "GET", url: "api/playlists/videos/:playlist_id", params: {playlist_id: "@_id"}, isArray: true},
        register:   {method: "POST", url: "api/playlists/"},
        addVideo:   {method: "POST", url: "api/playlists/:playlist_id/add/:video_id", params: {playlist_id: "@_id", video_id: "@_id"}}
    });

    return obj;
}]);


/**
 * Session manager
 */
services.factory("Session", ["$rootScope", function ($rootScope) {
    var session = {};
    var _user   = {};
    var _old    = {};
    var _name   = "sessionChange";


    /**
     * copy old session
     */
    var _copy = function () {
        _old = JSON.parse( JSON.stringify(_user) );
    };

    /**
     * Clean up the session
     */
    var _freshSession = function () {
        _user = {
            id: -1,
            authenticated: false,
            name: "",
            email: "",
            level: -1
        };
    };

    var _setSession = function (user) {
        /**
         * No changes
         */
        if (typeof user === "undefined") {
            _freshSession();
            return false;
        }

        /**
         * set values, if false|undefined then set standard value
         */
        _user.id            = user.id               || _user.id;
        _user.authenticated = user.authenticated    || _user.authenticated;
        _user.name          = user.name             || _user.name;
        _user.email         = user.email            || _user.email;
        _user.level         = user.level            || _user.level;



        /**
         * return  update successful
         */
        return true;
    };

    /**
     * sends a signal when a change happened
     */
    var _notify = function () {
        if (_old !== _user) {
            _copy();
            $rootScope.$emit(_name, "update!");
        }
    };


    /**
     * initiate a fresh session and store it into old for later checking
     */
    _freshSession();
    _copy();


    /**
     * generate session
     *
     * @param user
     */
    session.create = function (user) {
        _freshSession();

        _setSession(user);

        _notify();
    };

    /**
     * destroy session
     */
    session.destroy = function () {
        _freshSession();

        _notify();
    };

    /**
     * update session data
     *
     * @param user
     */
    session.update = function (user) {
        _setSession(user);

        _notify();
    };

    /**
     * Retrieves session data
     */
    session.content = function () {
        return _user;
    };

    /**
     * check if user is authenticated
     *
     * @returns {boolean}
     */
    session.authenticated = function () {
        return _user.authenticated;
    };

    /**
     * check if the user have the given role!
     *
     * @param roles
     * @returns {*|boolean}
     */
    session.authorized = function (roles) {
        if (!angular.isArray(roles)) {
            roles = [roles];
        }

        return (session.isAuthenticated() && roles.indexOf(_user.level) !== -1);
    };

    return session;
}]);