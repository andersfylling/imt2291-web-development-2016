<?php
require'./base.php';
?>
<!DOCTYPE HTML>
<html lang="nb">
<head>
    <title><?php echo c_project_name; ?></title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- base "/web-proj1" is also found in the navbar header title-->
    <base href="/<?php echo $_base; ?>">

    <!-- css -->
    <link href="public/components/angular-material/angular-material.min.css"    rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"        rel="stylesheet">
    <link href="public/css/main.css"                                           rel="stylesheet">

    <!-- global attributes -->
    <script type="text/javascript">
        var project = {"name": "<?php echo c_project_name; ?>"};
    </script>
</head>

<!--
    ng-style overwrites the style attribute after page load. Using this,
    the user won't see ugly not formatted angular js \ angular material code.
-->
<body
    layout="column"
    ng-style="{'visibility':'visible'}"
    ng-controller="AppCtrl"
    ng-app="videoApp"
    ng-cloak>

    <!--
        Site top menu.
        Always visible.
    -->
    <md-toolbar id="navbar" layout="row"
    >
        <div class="md-toolbar-tools"
        >

            <!--
                Side-menu button. visible for medium screens and below.
                This is also hidden if the search bar is avtive on mobile.
            -->
            <md-button
                ng-click="sidenav.toggle('left')"
                class="md-icon-button"
                aria-label="Settings"
                ng-hide="toolbar.search.visible"
                hide-gt-md
            >
                <md-icon class="material-icons"
                > menu
                </md-icon>
            </md-button>

            <!--
                Project title \ site name.
                Only hidden when the search bar is active on mobile.
            -->
            <h2 ng-hide="toolbar.search.visible"
            >
                <span
                > {{ ::toolbar.title }}
                </span>
            </h2>

            <!--
                Search bar for desktop.
                Mobile has a different one, this is hidden if the screen
                is small or narrower.
            -->
            <md-list
                flex-offset="10"
                flex
                hide-xs
                hide-sm
            >
                <md-list-item flex-offset="5" flex
                ><!-- BUG: weird bug with layout? -->
                    <md-input-container
                        md-no-float class="md-raised"
                        flex
                    >
                        <input
                            id="navbar-search-input"
                            ng-model="toolbar.search.input"
                            type="text"
                            placeholder="Search"
                            flex
                        >
                    </md-input-container>
                    <md-button
                        id=navbar-search-button"
                        class="md-raised md-mini md-primary md-cornered"
                    >
                        <md-icon class="material-icon"
                        > search
                        </md-icon>
                    </md-button>
                </md-list-item>
            </md-list>

            <!--
                Simple spacing to push everything after the
                search bar to the right.
            -->
            <span flex></span>

            <!--
                Right side icons \ menu. 4 elements.
                Both #1 and #2 are hidden for screen larger than smaller sizes.
            -->
            <!-- #1: go back & search input -->
            <div
                layout="row"
                layout-align="start center"
                ng-show="toolbar.search.visible"
                hide-gt-sm
            >
                <md-button
                    class="md-icon-button"
                    ng-click="toolbar.search.visible = !toolbar.search.visible"
                >
                    <md-icon class="material-icons"
                    > arrow_back
                    </md-icon>
                </md-button>

                <md-input-container md-no-float >
                    <input
                        ng-model="toolbar.search.input"
                        type="text"
                        placeholder="Search query"
                        ng-required="true"
                    >
                </md-input-container>
            </div>

            <!-- #2: search icon -->
            <md-button
                aria-label="Show search input"
                class="md-icon-button"
                ng-click="toolbar.search.visible = !toolbar.search.visible"
                ng-show="!toolbar.search.visible"
                hide-gt-sm
            >
                <md-icon class="material-icons"
                > search
                </md-icon>
            </md-button>

            <!-- #3: right side menu button -->
            <md-menu>
                <md-button class="md-icon-button"
                    aria-label="Open phone interactions menu"
                    ng-click="toolbar.menu.toggle($mdOpenMenu, $event)"
                >
                    <md-icon class="material-icons"
                    > apps
                    </md-icon>
                </md-button>

                <md-menu-content width="4"
                >
                    <!-- sign in -->
                    <md-menu-item ng-hide="isAuthenticated"
                    >
                        <md-button ng-click="goto('/signin')"
                        >
                            <md-icon class="material-icons"
                            > lock_open
                            </md-icon>

                            Sign in
                        </md-button>
                    </md-menu-item>

                    <!-- Upload video -->
                    <md-menu-item ng-hide="!isAuthenticated"
                    >
                        <md-button ng-click="goto('/dashboard/upload/video')"
                        >
                            <md-icon class="material-icons"
                            > file_upload
                            </md-icon>

                            Upload video
                        </md-button>
                    </md-menu-item>

                    <!-- My videos -->
                    <md-menu-item ng-hide="!isAuthenticated"
                    >
                        <md-button ng-click="goto('/dashboard/videos')"
                        >
                            <md-icon class="material-icons"
                            > video_library
                            </md-icon>

                            My videos
                        </md-button>
                    </md-menu-item>

                    <!-- My playlists -->
                    <md-menu-item ng-hide="!isAuthenticated"
                    >
                        <md-button ng-click="goto('/dashboard/playlists')"
                        >
                            <md-icon class="material-icons"
                            > list
                            </md-icon>

                            My playlists
                        </md-button>
                    </md-menu-item>

                    <!-- Dashboard -->
                    <md-menu-item ng-hide="!isAuthenticated"
                    >
                        <md-button ng-click="goto('/dashboard')"
                        >
                            <md-icon class="material-icons"
                            > dashboard
                            </md-icon>

                            Dashboard
                        </md-button>
                    </md-menu-item>

                    <!-- Sign out -->
                    <md-menu-item ng-hide="!isAuthenticated"
                    >
                        <md-button ng-click="goto('/signout')"
                        >
                            <md-icon class="material-icons"
                            > lock_outline
                            </md-icon>

                            Sign out
                        </md-button>
                    </md-menu-item>


                </md-menu-content>
            </md-menu>

            <!-- #4: user icon. Hide if not authenticated, or show as red? -->
            <md-button
                ng-disabled="true"
                class="md-icon-button md-primary md-raised"
            >
                <md-icon ng-hide="!isAuthenticated"
                    class="material-icons"
                    style="color:white"
                > face
                </md-icon>
                <md-icon ng-hide="isAuthenticated"
                    class="material-icons"
                > face
                </md-icon>
            </md-button>


        </div>
    </md-toolbar>
    <md-progress-linear md-mode="{{ mainProgressMode }}" value="{{ mainProgressValue }}" content-progressbar></md-progress-linear>
    <!-- nav bar end -->


    <!--
        Main content wrapper.
    -->
    <div layout="row" flex >
        <!-- sidenav menu -->
        <md-sidenav
            layout="column"
            class="md-sidenav-left md-whiteframe-z2"
            md-component-id="left"
            md-is-locked-open="$mdMedia('gt-md')"
            style="box-shadow:none;background-color: #FDFDFD"
        >
            <md-list>
                <!--
                    Defaults options in side menu.
                    Visible for both guests and users.
                -->
                <!-- home button -->
                <md-list-item ng-click="sidenav.toggle('left'); goto('/')"
                >
                    <md-icon class="material-icons"
                    > home
                    </md-icon>
                    <p
                    > Home
                    </p>
                </md-list-item>

                <!-- Video log -->
                <md-list-item ng-click="sidenav.toggle('left'); goto('/videolog')"
                >
                    <md-icon class="material-icons"
                    > view_list
                    </md-icon>
                    <p
                    > Videolog
                    </p>
                </md-list-item>

                <!-- Session list -->
                <md-list-item ng-click="sidenav.toggle('left'); goto('/session-list')"
                >
                    <md-icon class="material-icons"
                    > list
                    </md-icon>
                    <p
                    > Session list
                    </p>
                </md-list-item>


                <!--
                    This section will only be visible for users.
                -->
                <div ng-hide="!isAuthenticated"
                >
                    <!-- creates a divider between default and user content -->
                    <md-divider></md-divider>

                    <!-- "My videos" -->
                    <md-subheader class="md-no-sticky"
                    > My videos
                    </md-subheader>
                    <md-list-item
                        ng-repeat="item in sidenav.list.videos"
                        ng-click="sidenav.toggle('left'); goto('watch/' + item.id)"
                    >
                        <md-icon class="material-icons"
                        > video_library
                        </md-icon>
                        <p
                        > {{item.title | limitTo:sidenav.list.strLimit}}
                        </p>
                        <!--options button, Not enough time!
                        <md-button
                            class="md-icon-button"
                            ng-click="doSecondaryAction($event)"
                        >
                            <md-icon class="material-icons"
                            > more_vert
                            </md-icon>
                        </md-button>
                        -->
                    </md-list-item>
                    <md-divider></md-divider>

                    <!-- "My playlists" -->
                    <md-subheader class="md-no-sticky"
                    > My playlists
                    </md-subheader>
                    <md-list-item
                        ng-repeat="item in sidenav.list.playlists"
                        ng-click="sidenav.toggle('left'); goto('playlists/' + item.id)"
                    >
                        <md-icon class="material-icons"
                        > list
                        </md-icon>
                        <p
                        > {{item.title | limitTo:sidenav.list.strLimit}}
                        </p>
                        <!--options button, Not enough time!
                        <md-button
                            class="md-icon-button"
                            ng-click="doSecondaryAction($event)"
                        >
                            <md-icon class="material-icons"
                            > more_vert
                            </md-icon>
                        </md-button>
                        -->
                    </md-list-item>
                </div>

            </md-list>
        </md-sidenav>
        <!-- side nav menu end -->

        <!-- ng-view, template content -->
        <md-content
            flex
            layout-padding
            ng-view
        >
            <div layout-sm="column"
                layout-align="space-around"
            >
                <md-progress-circular md-mode="indeterminate"></md-progress-circular>
            </div>

        </md-content>
    </div>

    <!-- no footer needed! -->


    <!-- javascript -->
    <script type="text/javascript" src="public/components/angular/angular.js"></script>
    <script type="text/javascript" src="public/components/angular-route/angular-route.js"></script>
    <script type="text/javascript" src="public/components/angular-resource/angular-resource.js"></script>
    <script type="text/javascript" src="public/components/angular-sanitize/angular-sanitize.js"></script>
    <script type="text/javascript" src="public/components/angular-messages/angular-messages.js"></script>
    <script type="text/javascript" src="public/components/angular-aria/angular-aria.js"></script>
    <script type="text/javascript" src="public/components/angular-animate/angular-animate.js"></script>
    <script type="text/javascript" src="public/components/angular-material/angular-material.js"></script>

    <script type="text/javascript" src="public/js/app.js"></script>
    <script type="text/javascript" src="public/js/services.js"></script>
    <script type="text/javascript" src="public/js/directives.js"></script>
    <script type="text/javascript" src="public/js/controllers.js"></script>

</body>
</html>