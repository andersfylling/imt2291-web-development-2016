<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 20/04/16
 * Time: 08:11
 */

{
    $con        = true;
    $version    = -1;

    try
    {
        $pdo = new PDO(
            'mysql:host=' . c_database_host, c_database_user, c_database_pass,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
        );

        $version = $pdo->getAttribute(PDO::ATTR_SERVER_VERSION);
    }
    catch (PDOException $e)
    {
        $con = false;
    }

    echo '<b>Database connection:</b>' . "\t\t\t\t\t";
    if (!$con)
    {
        echo '<span style="color:red;">ERROR!</span> configure database credentials in /server/config/constats.php!';
        $success = false;
    }
    else
    {
        echo '<span style="color:green;">OK!</span>';
    }
    echo PHP_EOL;

    echo '<b>Database version:</b> <i>' . "\t" . $version . '</i>' . "\t\t";
    if ($version != version_database && $version !== -1)
    {
        echo '<span style="color:red;">ERROR!</span> Database version should be ' . version_database;
        $success = false;
    }
    else if ($version == -1)
    {
        echo "\t\t" . '<span style="color:red;">ERROR!</span> Database version could not be identified!';
        $success = false;
    }
    else
    {
        echo '<span style="color:green;">OK!</span>';
    }
    echo PHP_EOL;


}