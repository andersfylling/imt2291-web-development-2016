<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 20/04/16
 * Time: 06:44
 */

/**
 * Constants
 */
require_once 'server/config/constants.php'; //MySQL check
const version_php       = '7.0.4';
const working_php       = true;
const version_database  = '5.5.5-10.1.10-MariaDB';

const version_apache    = 'Apache/2.4.18';


echo '<pre>';

$success = true;

/**
 * Check if php is working
 */
if (!working_php)
{
    echo '<h2 style="color:red;font-weight:bold;">IF YOU CAN SEE THIS TEXT, THEN YOU DO NOT HAVE PHP WORKING! FIX THIS BEFORE PRECEDING!</h2>';
}

/* Confirm that the PHP version is 7.0.4 */
require_once 'php-version.php';

/* Confirm that apache version is 2.4.18 */
require_once 'apache-version.php';

/* Confirm that correct Apache modules are activated / installed */
require_once 'apache-modules-requirements-check.php';

/* Check if MySQL credentials are default, and verify MySQL version */
require_once 'mysql-check.php';

/* verify internet connection */
require_once 'internet-connection.php';

if ($success)
{
    echo PHP_EOL . PHP_EOL . PHP_EOL;
    echo 'Superb!' . PHP_EOL;
    echo 'Now cd into the "imt2911-web-development-2016"-folder and execute the command "bower install"!' . PHP_EOL;
    echo 'PS! Install bower using "npm install -g bower"';
}
else
{
    echo PHP_EOL . PHP_EOL . PHP_EOL;
    echo 'OH NO!!!' . PHP_EOL;
    echo 'Please fix the above errors before evaluating my work :)';
}