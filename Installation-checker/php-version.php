<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 20/04/16
 * Time: 07:02
 */

/* Confirm that the PHP version is const version_php */
{
    echo '<b>PHP version:</b> <i>' . "\t\t" . phpversion() . '</i>' . "\t\t\t\t";
    if (phpversion() != version_php)
    {
        echo  '<span style="color:red;">ERROR!</span> PHP should be version ' . version_php;
        $success = false;
    }
    else
    {
        echo '<span style="color:green;">OK!</span>';
    }
    echo PHP_EOL;
}
