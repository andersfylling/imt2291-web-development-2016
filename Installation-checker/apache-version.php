<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 20/04/16
 * Time: 07:03
 */

/* Confirm that apache version is 2.4.18 */
{
    echo '<b>Apache version:</b> <i>' . "\t" . substr(apache_get_version(), 0, 13) . '</i>' . "\t\t\t";
    if (substr(apache_get_version(), 0, 13) != version_apache)
    {
        echo '<span style="color:red;">ERROR!</span> should be ' . version_apache;
        $success = false;
    }
    else
    {
        echo '<span style="color:green;">OK!</span>';
    }
    echo PHP_EOL;
}