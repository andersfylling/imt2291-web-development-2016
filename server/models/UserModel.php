<?php

/**
 * User Model
 *
 * Requires:
 * Requires-classes:    Model.php
 *
 * Database Schema 'User':
 *  @column INT         id          unique, auto increment
 *  @column VARCHAR(30) email
 *  @column VARCHAR(60) password
 *  @column INT(3)      level
 *  @column TIMESTAMP   created     CURRENT_TIMESTAMP
 *  @column TIMESTAMP   updated     CURRENT_TIMESTAMP, on update CURRENT_TIMESTAMP
 *
 * @author  ANDERS OEN FYLLING, anders@nordic.email
 */
class UserModel extends \Frameworks\Model {

    function __construct () {
        parent::__construct('user');
    }

    public function authenticate ($data, $callback) {
        $res    = [];
        $code   = 409;  //conflict http code

        $stmt = $this->db->prepare('SELECT id,email,name,password,level FROM user WHERE email = :email');
        $stmt->execute([
            ':email'    => $data['email']
        ]);

        if ($stmt->rowCount() > 0 ) {	            // If user exists
            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            if (password_verify($data['password'], $user['password'])) {
                $res = [
                    'id'    => $user['id'],
                    'email' => $user['email'],
                    'name'  => $user['name'],
                    'level' => $user['level'],
                ];

                $code = 200; //if everything was ok set it to "ok"
            }
        }

        $this->callback($callback, $res, $code);
    }

    public function update ($data, $callback) {
        $res    = [];
        $code   = 500;  //conflict http code

        $stmt = $this->db->prepare('UPDATE user
                                    SET email     = :email,
                                        name      = :name,
                                        password  = :password,
                                        level     = :level
                                    WHERE id = :id');
        $stmt->execute([
            ':email'    => $data['email'],
            ':name'     => $data['name'],
            ':password' => $data['password'],
            ':level'    => $data['level'],
            ':id'       => $data['id']
        ]);

        if ($stmt->rowCount() > 0 ) { //if updated
            $code = 200; //if everything was ok set it to "ok"
            $res = $data;
        }

        $this->callback($callback, $res, $code);
    }

}
