<?php

/**
 * Playlist Model
 *
 * Requires:
 * Requires-classes:    Model.php
 *
 * Database Schema 'Playlist': TODO: fix this info
 *  @column INT         id          unique, auto increment
 *  @column VARCHAR(30) email
 *  @column VARCHAR(60) password
 *  @column INT(3)      level
 *  @column TIMESTAMP   created     CURRENT_TIMESTAMP
 *  @column TIMESTAMP   updated     CURRENT_TIMESTAMP, on update CURRENT_TIMESTAMP
 *
 * @author  ANDERS OEN FYLLING, anders@nordic.email
 */
class PlaylistModel extends \Frameworks\Model {

    function __construct () {
        parent::__construct('playlist'); //database table name
    }

    public function withUserID ($data, $callback) {
        $res    = [];
        $code   = 204;  // no content

        $stmt = $this->db->prepare('SELECT * FROM playlist WHERE user_id = :user_id');
        $stmt->execute([
            ':user_id'    => $data['id']
        ]);

        if ($stmt->rowCount() > 0 ) {	            // If found
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $code = 200; //if everything was ok set it to "ok"
        }

        $this->callback($callback, $res, $code);
    }

    public function getVideos ($data, $callback) {
        $res    = [];
        $code   = 204;  // no content

        $stmt = $this->db->prepare('SELECT playlist_id,video_id FROM playlist_video WHERE playlist_id = :playlist_id');
        $stmt->execute([
            ':playlist_id'    => $data['id']
        ]);

        if ($stmt->rowCount() > 0 ) {	            // If found
            $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($videos as $e) {
                $stmt = $this->db->prepare('SELECT * FROM video WHERE id = :id');
                $stmt->execute([
                    ':id'    => $e['video_id']
                ]);

                if ($stmt->rowCount() > 0 ) {                // If found
                    $res[] = $stmt->fetch(PDO::FETCH_ASSOC);
                    $code = 200; //if everything was ok set it to "ok"
                }
            }

        }

        $this->callback($callback, $res, $code);
    }

    public function addVideo ($data, $callback) {
        $res    = [];
        $code   = 500;  // server error

        $stmt = $this->db->prepare('INSERT INTO playlist_video (playlist_id, video_id) VALUES (:playlist_id, :video_id)');
        $insertet = $stmt->execute([
            ':playlist_id'  => $data['playlist_id'],
            ':video_id'     => $data['video_id']
        ]);

        if ($insertet) { //ok
            $code = 200;
            $res[] = $this->db->lastInsertId();
        }

        $this->callback($callback, $res, $code);
    }

}
