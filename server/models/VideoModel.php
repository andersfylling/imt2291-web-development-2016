<?php

class VideoModel extends \Frameworks\Model {
    function __construct() {
        parent::__construct('video');
    }

    public function upload ($data, $callback) {
        $this->insert($data, function ($res, $code) use ($callback) {
            //TODO: get a image from videofile or the one user uploaded
            //TODO: fix..
            if ($code === '201') { //201 == created

                $file = rtrim(c_video_folder, '/') . '/' . $res['id'];
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $file)) {
                    $this->callback($callback, $res, $code);

                } else {
                    $this->remove($res['id'], function () use ($callback) {
                        $this->callback($callback, ['msg' => 'couldnt upload. deleted from record.'], 500);
                    });
                    $this->callback($callback, [], 500);
                }
            } else {
                $this->callback($callback, $res, $code);
            }

        });
    }

    /**
     * Gets all the videos from a given playlist id
     *
     * @param $data
     * @param $callback
     */
    public function getFromPlaylist ($data, $callback) {
        //TODO: fill in video->getFromPlaylist
    }

    /**
     * Returns an array of playlists that contains the video id
     *
     * @param $data
     * @param $callback
     */
    public function getPlaylists ($data, $callback) {
        //TODO: fill in video->getPlaylists
    }

    //public function
}
