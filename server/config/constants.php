<?php


/**
 * User levels
 */
const c_administrator_level = 2;
const c_user_level          = 1;
const c_guest_level         = 0;


/**
 * Other project values
 */
const c_project_name        = 'vLearner';
const c_video_format        = 'mp4';



/**
 * declare constants used by \Frameworks
 */
//paths
const c_models_path         = 'server/models/';
const c_controllers_path    = 'server/controllers/';
const c_video_folder        = 'server/videos/';

//database type=mysql
const c_database_host       = '127.0.0.1';
const c_database_name       = 'project1';
const c_database_user       = 'root';
const c_database_pass       = '';
const c_database_file       = 'server/config/database.sql';

//session
const c_session_timeout     = 64800;    // 18 hours