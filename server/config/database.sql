-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE IF NOT EXISTS `playlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`user_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_playlist_user1_idx` (`user_id`),
  CONSTRAINT `fk_playlist_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

INSERT INTO `playlist` (`id`, `title`, `description`, `user_id`, `created`, `updated`) VALUES
(1,	'testing',	'tester',	1,	'2016-02-28 14:53:22',	'2016-02-28 14:53:22'),
(2,	'anders',	'test2',	1,	'2016-03-06 22:00:07',	'2016-03-06 22:00:07')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `title` = VALUES(`title`), `description` = VALUES(`description`), `user_id` = VALUES(`user_id`), `created` = VALUES(`created`), `updated` = VALUES(`updated`);

CREATE TABLE IF NOT EXISTS `playlist_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `index` varchar(45) NOT NULL,
  `video_id` int(11) NOT NULL,
  `playlist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`video_id`,`playlist_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `index_UNIQUE` (`index`),
  KEY `fk_playlist_video_video_idx` (`video_id`),
  KEY `fk_playlist_video_playlist1_idx` (`playlist_id`),
  CONSTRAINT `fk_playlist_video_playlist1` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlist_video_video` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `playlist_video` (`id`, `created`, `updated`, `index`, `video_id`, `playlist_id`) VALUES
(1,	'2016-02-28 14:53:38',	'2016-02-28 14:53:38',	'1',	137,	1),
(2,	'2016-03-06 22:30:24',	'2016-03-06 22:30:24',	'',	137,	2)
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `created` = VALUES(`created`), `updated` = VALUES(`updated`), `index` = VALUES(`index`), `video_id` = VALUES(`video_id`), `playlist_id` = VALUES(`playlist_id`);

CREATE TABLE IF NOT EXISTS `tablesexists` (
  `id` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='just to check if tables has been generated';

INSERT INTO `tablesexists` (`id`) VALUES
(1)
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`);

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 NOT NULL,
  `level` int(3) NOT NULL DEFAULT '1' COMMENT '1=user',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(25) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

INSERT INTO `user` (`id`, `email`, `password`, `level`, `created`, `updated`, `name`) VALUES
(1,	'admin@admin.net',	'$2y$10$gibmtQwaiqlMXn2eJVAz2uxhD1ikmOGDTz32XSzjXDr/NPKpI9w82',	2,	'2016-02-28 14:51:52',	'2016-03-06 20:49:21',	'admin'),
(2,	'anders@nordic.email',	'$2y$10$qdY/yTAVXRwdbjWX8GLR6eof7Dbt7U4piyPcJ.4CNLkZ.qW/A9Sp6',	1,	'2016-03-06 19:55:09',	'2016-03-06 19:55:09',	'anders'),
(3,	'test2@yey.com',	'$2y$10$6bxDj1NTkg/2RiVcJtkKHeNCYs6pvjG1DNllgBkCyTEgXdLUiPe0W',	1,	'2016-03-06 19:57:00',	'2016-03-06 19:57:00',	'test')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `email` = VALUES(`email`), `password` = VALUES(`password`), `level` = VALUES(`level`), `created` = VALUES(`created`), `updated` = VALUES(`updated`), `name` = VALUES(`name`);

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL,
  `title` varchar(45) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `transcript_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`user_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `transcript_id_UNIQUE` (`transcript_id`),
  KEY `fk_video_user_idx` (`user_id`),
  CONSTRAINT `fk_video_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

INSERT INTO `video` (`id`, `title`, `description`, `user_id`, `transcript_id`, `created`, `updated`) VALUES
(137,	'Ut i vår hage 2 - Gress',	'video av ut i vår hage 2',	1,	1,	'2016-02-28 14:52:58',	'2016-02-28 14:52:58')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `title` = VALUES(`title`), `description` = VALUES(`description`), `user_id` = VALUES(`user_id`), `transcript_id` = VALUES(`transcript_id`), `created` = VALUES(`created`), `updated` = VALUES(`updated`);

-- 2016-03-06 22:48:42