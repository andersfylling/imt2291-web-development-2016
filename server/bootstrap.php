<?php

/**
 * Add our application based constants
 * Add Frameworks [router, model, controller, database, session]
 */
require 'config/constants.php';
require 'Frameworks/bootstrap.php';



/**
 * Require and initiate Router
 */
$Router = new Frameworks\Router(
    'api',                  // api prefix, default = api
    'public/index.php',     // location for SPA file, default = 'SPA.php'
    [],                     // http methods, defaults = [GET, HEAD, PUT, POST, DELETE]
    TRUE,                   // Activate Session for the router, defaults = FALSE
    FALSE                   // Use CSRF tokens on each request, logout - login with videoStream bug!, defaults = FALSE
);

// Routes
require 'routes.php';