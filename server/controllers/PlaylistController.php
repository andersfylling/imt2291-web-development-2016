<?php

/**
 * Controller Playlists
 *
 * @author  ANDERS OEN FYLLING, anders@nordic.email
 */
class PlaylistController extends \Frameworks\Controller {

    function __construct() {
        parent::__construct('PlaylistModel');
    }

    /**
     * Returns all the videos inside data array as json objects
     *
     * @param $req
     * @param $res
     */
    public function getAll ($req, $res) {
        $this->model->find([], function ($data, $code) use ($res) {
            $res->json($data, $code);
        });
    }

    /**
     * Retrieves information about a certain playlist with given id.
     *  if the data array is empty, the playlist was not found.
     *
     * @param $query
     * @param $body
     */
    public function getID ($req, $res) {
        $this->model->find(['id' => $req->get_param('id')], function ($data, $code) use ($res) {
            $res->json($data, $code);
        });
    }

    /**
     * Retrieves a specific users list
     *
     * @param $req
     * @param $res
     */
    public function getListOfUser ($req, $res) {
        $this->model->withUserID(['id' => $req->get_param('id')], function ($data, $code) use ($res) {
            $res->json($data, $code);
        });
    }

    /**
     * Retrieves videos within playlist
     *
     * @param $req
     * @param $res
     */
    public function getVideos ($req, $res) {
        $this->model->getVideos(['id' => $req->get_param('id')], function ($data, $code) use ($res) {
            $res->json($data, $code);
        });
    }

    /**
     * Retrieves videos within playlist
     *
     * @param $req
     * @param $res
     */
    public function register ($req, $res) {
        /**
         * check if user is authenticated
         */
        if ($req->session->get('authenticated') === FALSE) {
            $res->failure(401); //not authorized
        }

        /**
         * Insert user object to database!
         */
        $this->model->insert($req->get_body(), function ($data, $code) use ($res) {
            $res->json($data, $code);
        });
    }

    /**
     * add a video to playlist
     *
     * @param $req
     * @param $res
     */
    public function addVideo ($req, $res) {
        /**
         * check if user is authenticated
         */
        if ($req->session->get('authenticated') === FALSE) {
            $res->failure(401); //not authorized
        }

        /**
         * Insert video to playlist
         */
        $this->model->addVideo($req->get_body(), function ($data, $code) use ($res) {
            $res->json($data, $code);
        });
    }

}
