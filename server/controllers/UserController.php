<?php

/**
 * Controller Users
 *
 * @author  ANDERS OEN FYLLING, anders@nordic.email
 */
class UserController extends \Frameworks\Controller {

    function __construct() {
        parent::__construct('UserModel');
    }

    /**
     * Returns all users.
     *  Using the foreach loop, each password row is unset before sending.
     *
     * @param $req
     * @param $res
     */
    public function getAll ($req, $res) {
        $this->model->find([], function ($data, $code) use ($res) {
            foreach ($data as &$user) {
                unset($user['password']);
            }

            $res->json($data, $code);
        });
    }

    /**
     * Returns details from session
     *
     * @param $query
     * @param $body
     */
    public function getSession ($req, $res) {
        $res->json($req->session->getUserConnection());
    }

    /**
     * Returns details from profile
     *
     * @param $query
     * @param $body
     */
    public function getProfile ($req, $res) {
        if ($req->session->get('authenticated') === FALSE) {
            $res->failure(401); //not authorized
        }

        $this->model->find(
            ['id' => $req->session->get('id')],
            function ($data, $code) use ($res) {
                $res->json($data, $code);
            });
    }

    /**
     * Retrieves information about a certain user with given id.
     *  if the data array is empty, the user was not found.
     *
     * @param $query
     * @param $body
     */
    public function getID ($req, $res) {
        $this->model->find(
            ['id' => $req->get_param('id')],
            function ($data, $code) use ($res) {
                $res->json($data, $code);
        });
    }

    /**
     * Registers a user. Its required to be an administrator and that all the details
     *  is given within the body. the rest will be ignored.
     *
     * @param $query
     * @param $body
     */
    public function register ($req, $res) {
        /**
         * check if user is authenticated
         */
        if ($req->session->get('authenticated') === FALSE) {
            $res->failure(401); //not authorized
        }

        /**
         * check if user is admin
         */
        if ($req->session->get('level') < c_administrator_level) {
            $res->failure(401); //not authorized. User must be admin
        }

        /**
         * Insert user object to database!
         */
        $this->model->insert($req->get_body(), function ($data, $code) use ($res) {
            $res->json($data, $code);
        });
    }

    /**
     * Authenticates a user logging in. must pass the data through post as get is not supported!
     *
     * @param $query
     * @param $body
     */
    public function login ($req, $res) {
        if ($req->session->get('authenticated') === FALSE) {
            $email      = $req->get_body('email');
            $password   = $req->get_body('password');

            if ($email && $password) {
                $this->model->authenticate($req->get_body(), function ($data, $code) use ($req, $res) {
                    $req->session->set($data); //array of user data
                    $req->session->set('authenticated', TRUE);

                    $res->json($data, $code);
                });
            } else {
                $res->json(['msg' => 'Email \ Password is missing.']);
            }
        } else {
            $res->json(['msg' => 'You are already logged in.']);
        }
    }

    /**
     * Deauthenticates the user. -> logs out
     */
    public function logout ($req, $res) {
        if ($req->session->get('authenticated') === TRUE) {
            $req->session->destroy();
            $res->success(200);
        } else {
            $res->failure(401); // not authorized
        }
    }


    /**
     * Updates a current user.
     *
     * @param $query
     * @param $body
     */
    public function update ($req, $res) {
        if ($req->session->get('authenticated') === FALSE) {
            $res->failure(401); //not authorized
        }

        /**
         * copy request body
         */
        $body = $req->get_body();

        /**
         * check that password and level is present
         */
        $body['password']   = isset($body['password'])  ? $body['password'] : NULL;
        $body['level']      = isset($body['level'])     ? $body['level']    : NULL;

        /**
         * hash password if set
         */
        if ($body['password'] !== NULL) {
            $body['password'] = password_hash($body['password'], PASSWORD_DEFAULT);
        }

        /**
         * if the user is not an administrator, don't let the user update user levels!
         */
        if ($req->session->get('level') < c_administrator_level && $body['level'] > 1) {
            $body['level'] = NULL;
        }

        /**
         * update model
         * user_id is in request body
         */
        $this->model->update($body, function ($data, $code) use ($req, $res) {

            //if its an update from the profile, we want to update the session as well
            if ($req->session->get('id') === $data['id']) {
                unset($data['password']);
                $req->session->set($data); //array of user data
            }

            $res->json($req->session->getUserConnection(), $code);
        });
    }

    public function delete ($query, $body) {
        if (Session::get('level') === ADMINLEVEL) {
            if (isset($query['id'])) {
                $this->model->delete($query, function ($res) {
                    Response::ok($res);
                });
            } else {
                Response::notAcceptable();
            }
        } else if (Session::get('authenticated') === true) {
            Response::unauthorized();
        } else {
            Response::unauthorized();
        }
    }

}
