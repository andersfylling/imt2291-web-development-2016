<?php

/**
 * Controller Videos
 *
 * @author  ANDERS OEN FYLLING, anders@nordic.email
 */
class VideoController extends \Frameworks\Controller {

    function __construct() {
        parent::__construct('VideoModel');


    }

    /**
     * Returns all the videos inside data array as json objects
     */
    public function getAll ($req, $res) {
        $this->model->find([], function ($data, $code) use ($res) {
            $res->json($data, $code);
        });
    }

    /**
     * Retrieves information about a certain video with given id.
     *  if the data array is empty, the video was not found.
     *
     * @param $query
     * @param $body
     */
    public function getID ($req, $res) {
        $this->model->find(
            ['id' => $req->get_param('id')],
            function ($data, $code) use ($res) {
                $res->json($data, $code);
        });
    }

    /**
     * Streams the video back the client.
     *
     * @param $request
     * @param $body
     */
    public function stream ($req, $res) {
        $video = c_video_folder . $req->get_param('id') . '.mp4';

        if (file_exists($video)) {
            $res->videoStream($video);
        } else {
            $res->failure(204); // no content
        }
    }

    public function fromPlaylist ($req, $res) {

    }

    public function tracks ($req, $res) {
        $directory = 'public/vtt/' . $req->get_param('id') . '/';
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));

        $res->json($scanned_directory);
    }

}
