<?php


/**
 * Routes goes here
 */
$Router->state('/users', function ($get, $head, $post, $put, $delete) {
    $get('/',                    'UserController.getAll');
    $get('/?[0-9]:id',           'UserController.getID');
    $get('/profile',             'UserController.getProfile');
    $get('/session',             'UserController.getSession');

    $head('/logout',             'UserController.logout');

    $post('/login',              'UserController.login');
    $post('/register',           'UserController.register');

    $put('/?[0-9]:id',           'UserController.update');
});

$Router->state('/videos', function ($get, $head, $post, $put, $delete) {
    $get('/',                    'VideoController.getAll');
    $get('/?[0-9]:id',           'VideoController.getID');
    $get('/stream/?[0-9]:id',    'VideoController.stream');
    $get('/?[0-9]:id/tracks',    'VideoController.tracks');

    $put('/upload',              'VideoController.upload');
});

$Router->state('/playlists', function ($get, $head, $post, $put, $delete) {
    $get('/',                    'PlaylistController.getAll');
    $get('/?[0-9]:id',           'PlaylistController.getID');
    $get('/users/?[0-9]:id',     'PlaylistController.getListOfUser');
    $get('/videos/?[0-9]:id',    'PlaylistController.getVideos');

    $post('/',                   'PlaylistController.register');
    $post('/add',                'PlaylistController.addVideo');
});


/**
 * At the end always call the garbage collector to report with a valid not found response.
 *  Otherwise the client will get a 200 response saying everything is okay
 */
$Router->garbageCollector();