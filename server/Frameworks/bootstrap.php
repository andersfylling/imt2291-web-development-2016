<?php

/**
 * Require classes \ traits \ utils
 */
require 'http/utils/content.php';
require 'http/utils/methods.php';
require 'http/utils/Session.php';       // Initiated in Request.php
require 'http/utils/VideoStream.php';

/**
 * Require the main classes used in controllers
 */
require 'http/Request.php';
require 'http/Response.php';

/**
 * Require the base classes
 */
require 'Database.php';                 // Initiated in Model by construct call
require 'Router.php';
require 'Model.php';
require 'Controller.php';