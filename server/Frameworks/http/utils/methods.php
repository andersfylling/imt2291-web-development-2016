<?php

namespace Frameworks\http\utils;

trait methods {

    /**
     * https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
     */
    private $methods_arr = [
        'GET'           => 1,
        'HEAD'          => 1,
        'POST'          => 1,
        'PUT'           => 1,
        'DELETE'        => 1,
        'OPTIONS'       => 0,
        'PATCH'         => 0,
        'TRACE'         => 0,
        'CONNECT'       => 0
    ];

    protected function methods_toString () {
        $string = '';

        foreach ($this->methods_arr as $key => $value) {
            if ($value === 1) {
                $string .= $key . ', ';
            }
        }

        return rtrim($string, ', ');
    }

    protected function methods_addMethod ($method, $value = 1) {
        $method = strtoupper($method);
        return $this->methods_arr[$method] = $value === 0 ? 0 : 1;
    }

    protected function methods_activateMethod ($method) {
        $method = strtoupper($method);
        return $this->methods_arr[$method] = 1;
    }

    protected function methods_deactivateMethod ($method) {
        $method = strtoupper($method);
        return $this->methods_arr[$method] = 0;
    }

    protected function methods_methodNotAllowed ($method) {
        $method = strtoupper($method);
        return !$this->methods_arr[$method];
    }

    protected function methods_methodNotKnown ($method) {
        $method = strtoupper($method);
        return !isset($this->methods_arr[$method]);
    }

}