<?php

namespace Frameworks\http\utils;

trait content {

    private $content_type_arr = [
        'pdf'   => 'application/pdf',
        'txt'   => 'text/plain; charset: UTF-8',
        'php'   => 'text/plain; charset: UTF-8',
        'html'  => 'text/html; charset: UTF-8',
        'css'   => 'text/css; charset: UTF-8',
        'json'  => 'application/json; charset: UTF-8',
        'mp4'   => 'video/mp4',
        'gif'   => 'image/gif',
        'png'   => 'image/png',
        'jpg'   => 'image/jpg'
    ];

    private $content_upload_type_arr = [
        'video/mp4' => 1
    ];


    protected function content_toString () {
        $string = '';

        foreach ($this->content_type_arr as $key => $value) {
            $string .= $value . ', ';
        }

        return rtrim($string, ', ');
    }

    protected function content_isAllowed ($type) {
        $result = '';   //returns the not allowed content type

        /**
         * First remove anything besides the content type
         *  eg. text/plain; charset: UTF-8 => text/plain
         *
         * Then flip the array for simpler
         */
        $arr = array_map(function ($type) {
            return explode(';', $type)[0];
        }, $this->content_type_arr);
        $arr = array_flip($arr);

        // Test if the type is allowed
        if (is_string($type) === TRUE) {
            if (isset($this->content_upload_type_arr[$type]) === FALSE) {
                $result = $type;
            }
        } else if (is_array($type) === TRUE) {
            foreach ($type as $t) {
                if (isset($this->content_upload_type_arr[$t['type']]) === FALSE) {
                    $result = $t['type'];
                    break;
                }
            }
        }

        //return the results, TRUE or content type
        if ($result === '') {
            return TRUE;
        } else {
            return $result;
        }
    }

    protected function content_getType ($trivial = '') {
        $content = 'txt';

        if (isset($this->content_type_arr[$trivial]) === TRUE) {
            $content = $this->content_type_arr[$trivial];
        } else {
            $content = $this->content_type_arr[$content];
        }

        return $content;
    }

}