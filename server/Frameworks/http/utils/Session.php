<?php

namespace Frameworks\http\utils;


class Session {

    /**
     * @var string key name of the users last activity int value.
     */
    private $last_request;

    /**
     *  Initiates the session startup and activates the timeout function for session.
     *
     * @param string    $last_request_name
     */
    function __construct (
        $last_request_name = 'last_request'
    ) {
        $this->last_request = $last_request_name;

        /**
         * Initiate session
         */
        session_start();
        $this->set('authenticated', false, false);

        /**
         * Checks if the user have been inactive for too long
         *  See constants.php for value in seconds
         *
         * If FALSE is returned by get, the compiler ignores it, and the if statement
         *  will look like (time() > c_session_timeout)
         */
        if (time() - $this->get($this->last_request) > c_session_timeout + time()) {
            $this->destroy();   // log out
        }

        /**
         * Update the time the last request was sent from the user.
         */
        $this->set($this->last_request, time());
    }

    /**
     * Formats the string parameter to desired lower|upper case
     *
     * @param string $str
     * @return string
     */
    private function format($str = '') {
        return strtolower($str);
    }

    /**
     * Sets a value to a key. For overwrite, if its not true, don't write if value exists
     *
     * @param string                $key
     * @param string|int|bool|float $value
     * @param bool                  $overwrite
     */
    public function set ($key, $value = '', $overwrite = TRUE) {
        if (gettype($key) === "array") {
            foreach ($key as $k => $v) {
                $_SESSION[$this->format($k)] = $v;
            }
        } else if ($overwrite === TRUE) {
            $_SESSION[$this->format($key)] = $value;
        } else if ($this->get($key) === FALSE) {
            $_SESSION[$this->format($key)] = $value;
        }
    }


    /**
     * WARNING: returns FALSE if the value is not set!
     *
     * Developer can search for content to check if it exists, $isValue must be true then.
     *
     * @param $data
     * @param bool $isValue
     * @return null
     */
    public function get ($data,$isValue = FALSE) {
        $r = FALSE;
        if ($isValue === FALSE) {
            $r = isset($_SESSION[$this->format($data)]) == TRUE ? $_SESSION[$this->format($data)] : FALSE;
        } else if (sizeof($_SESSION) > 0) {
            foreach ($_SESSION as $key => $value) {
                if ($data === $value) {
                    $r = $value;
                    break;
                }
            }
        }

        return $r;
    }

    /**
     * Returns the complete session
     *
     * @return mixed
     */
    public function getUserConnection () {
        $session = $_SESSION;
        unset($session[$this->last_request]);

        return $session;
    }

    /**
     * For setting an multidimensional array
     *
     * Source:
     *  https://www.sitepoint.com/community/t/accessing-a-session-var-in-a-php-class-object/7412/6
     *
     * @param $keys
     * @param $value
     */
    public function setMd($keys, $value) {
        $keys = array_map('format', $keys);
        $keysStr = "['".implode("']['", $keys)."']";
        $_SESSION{$keysStr} = $value;
    }

    /**
     * Unset and destroys session
     */
    public function destroy () {
        $_SESSION = [];                         // Unset session variables
        session_destroy();                      // Destroy it
    }

    /**
     * Call after done writing to session, and always before location header.
     * This can help to speed up the backend..
     */
    public function done () {
        session_write_close();
    }
}