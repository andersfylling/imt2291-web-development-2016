<?php

namespace Frameworks\http;

class Response extends utils\VideoStream {
    use utils\content, utils\methods;

    /**
     * Consult https://developer.mozilla.org/en-US/docs/Web/HTTP/Response_codes
     *  For information about each one.
     *
     * I recommend removing every code that you do not need. I have marked those
     *  I view as common once on the web 23.01.2016
     *
     * I designed the array with two varaibles \ index per line since its a long list
     *
     * @var array $httpCodes
     */
    private $httpCodes = [
        /* Informational Responses */
        'continue'                      => 100,                 'switchingProtocol'             => 101,
        /* Successful Responses */
        'ok'                            => 200, /* Common */    'created'                       => 201,
        'accepted'                      => 202, /* Common */    'nonAuthoritativeInformation'   => 203,
        'noContent'                     => 204, /* Common */    'resetContent'                  => 205,
        'partialContent'                => 206, /* Common */
        /* Redirection Messages */
        'multipleChoice'                => 300,                 'movedPermanently'              => 301, /* Common */
        'found'                         => 302,                 'seeOther'                      => 303,
        'notModified'                   => 304, /* Common */    'useProxy'                      => 305,
        'unused'                        => 306,                 'temporaryRedirect'             => 307, /* Common */
        'permanentRedirect'             => 308, /* Common */
        /* Client Error Responses */
        'badRequest'                    => 400, /* Common */    'unauthorized'                  => 401, /* Common */
        'paymentRequired'               => 402, /* Common */    'forbidden'                     => 403, /* Common */
        'notFound'                      => 404, /* Common */    'methodNotAllowed'              => 405, /* Common */
        'notAcceptable'                 => 406, /* Common */    'proxyAuthenticationRequired'   => 407,
        'requestTimeout'                => 408, /* Common */    'conflict'                      => 409,
        'gone'                          => 410,                 'lengthRequired'                => 411,
        'preconditionFailed'            => 412,                 'payloadTooLarge'               => 413,
        'URITooLong'                    => 414,                 'unsupportedMediaType'          => 415, /* Common */
        'requestedRangeNotSatisfiable'  => 416,                 'expectationFailed'             => 417,
        'imATeapot'                     => 418,                 'misdirectedRequest'            => 421,
        'upgradeRequired'               => 426,                 'preconditionRequired'          => 428,
        'tooManyRequests'               => 429, /* Common */    'requestHeaderFieldsTooLarge'   => 431,
        /* Server Error Responses */
        'internalServerError'           => 500, /* Common */    'notImplemented'                => 501, /* Common */
        'badGateway'                    => 502,                 'serviceUnavailable'            => 503,
        'gatewayTimeout'                => 504,                 'HTTPVersionNotSupported'       => 505,
        'variantAlsoNegotiates'         => 506,                 'variantAlsoNegotiates2'        => 507,
        'networkAuthenticationRequired' => 511,                 'permissionDenied'              => 550,
        /* Custom Responses */
    ];

    function __construct() {
        parent::__construct();
    }


    private function setResponseCode ($code = 200) {
        if ($code !== 200) {
            if (is_int($code) === TRUE && in_array($code, $this->httpCodes)) {
                //$code is valid
            } else if (is_string($code) && isset($this->httpCodes[$code]) === TRUE) {
                //key name was used, set code to its value
                $code = $this->httpCodes[$code];
            } else {
                $code = 200; //fallback code
            }
        }

        http_response_code($code);
    }

    private function addHeaders ($headers) {
        foreach ($headers as $header) {
            header($header);
        }
    }

    private function setHeaders ($type = NULL, $length = NULL, $size = NULL) {
        header('Access-Control-Allow-Methods: ' . $this->methods_toString());
        header('Cache-Control: public, max-age=0, no-cache');
        header('Access-Control-Allow-Credentials: true');

        if ($type !== NULL) {
            header('Content-Type: ' . $this->content_getType($type));

            if ($length !== NULL) {
                header('Content-Length: ' . $length);
            }
        }
    }

    /**
     * Public methods
     */

    /**
     * Method that only sends success codes
     *
     * @param int       $code
     */
    public function success($code = 200) {
        $this->setResponseCode($code);
        exit;
    }

    /**
     * Method that sends a string as response
     *
     * @param string    $res
     * @param int       $code
     * @param bool      $privateCache
     */
    public function send($res = '', $code = 200, $privateCache = false) {
        $type = 'txt';

        /**
         * convert response to a string type
         */
        $res = (string)$res;


        $this->setResponseCode($code);
        $this->setHeaders($type, strlen($res));

        /**
         * send string data and exit
         * TODO:implement own private method
         */
        echo $res;
        exit;
    }


    public function json($res = [], $code = 200, $privateCache = false) {
        $type = 'json';

        /**
         * convert response to a json type
         */
        $res = json_encode($res);

        /**
         * Set headers
         */
        $this->setResponseCode($code);
        $this->setHeaders($type, strlen($res));

        /**
         * send json data and exit
         */
        echo $res;
        exit;
    }

    public function failure($code = 500, $headers = []) {


        $this->setResponseCode($code);
        $this->addHeaders($headers);
        $this->setHeaders();

        /**
         * send string data and exit
         */
        exit;
    }

    /**
     * Sends a text file (.txt extension)
     *
     * @param string    $file   Requires full path to file (or relative, base is set by .htaccess)
     * @param int       $code
     */
    public function text($file = '', $code = 200) {
        $type = 'txt';

        /**
         * Checks if file exists
         */
        if (file_exists($file) === FALSE) {
            $code = 404;
            $type = NULL;
        }

        /**
         * Set headers
         */
        $this->setResponseCode($code);
        $this->setHeaders($type);

        /**
         * Check if the file exists and require it if so
         *  otherwise set a error header
         */
        if ($code !== 404) {
            require $file;
        }

        /**
         * exit script
         */
        exit;
    }

    public function html($file = '', $code = 200) {
        $type = 'html';

        /**
         * Checks if file exists
         */
        if (file_exists($file) === FALSE) {
            $code = 404;
            $type = NULL;
        }

        /**
         * Set headers
         */
        $this->setResponseCode($code);
        $this->setHeaders($type);

        /**
         * Check if the file exists and require it if so
         *  otherwise set a error header
         */
        if ($code !== 404) {
            require $file;
        }

        /**
         * exit script
         */
        exit;
    }

    /**
     * Streams a video file
     *
     * @author Rana
     * @Rewritten-by Anders Øen Fylling
     * @link http://codesamplez.com/programming/php-html5-video-streaming-tutorial
     *
     * @param $file
     * @param int $code
     */
    public function videoStream($file, $code = 200) {
        $this->start($file);
    }

}