<?php

namespace Frameworks\http;

class Request {
    use utils\content, utils\methods;

    private $api;       //api call
    private $method;    //request method
    private $params;    //request params from the uri
    private $body;      //request body
    private $headers;   //request headers
    private $files;     //files
    private $server;    //server

    private $errors;    //errors

    public $session;


    private function sanitizedArray ($type, $filter) {
        $_arr = [];

        if (is_array($type) === TRUE) {
            foreach ($type as $key => $value) {
                $_arr[$key] = filter_var($value, $filter);
            }
        } else if (is_string($type) === TRUE) {
            switch ($type) {
                case 'INPUT_GET' :    $_arr = $_GET;      unset($_GET);       break;
                case 'INPUT_POST' :   $_arr = $_POST;     unset($_POST);      break;
                case 'INPUT_COOKIE' : $_arr = $_COOKIE;   unset($_COOKIE);    break;
                case 'INPUT_SERVER' : $_arr = $_SERVER;   unset($_SERVER);    break;
                case 'INPUT_ENV' :    $_arr = $_ENV;      unset($_ENV);       break;
            }

            foreach ($_arr as $key => $value) {
                $_arr[$key] = filter_input($type, $key, $filter);
            }
        }

        return $_arr;
    }

    private function defineMethod () {
        $method = '';

        if (isset($_POST['HTTP_REQUEST_METHOD']) === TRUE) {
            $method = filter_input(INPUT_POST, 'HTTP_REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS);
            $method = strtoupper($method);
            unset($_POST['HTTP_REQUEST_METHOD']);
        } else {
            $method = filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS);
        }

        return $method;
    }

    private function defineParams () {
        $params = [];

        if (empty($_GET) === FALSE) {
            foreach ($_GET as $key => $value) {
                $params[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
                unset($_GET[$key]);
            }
        }

        return $params;
    }

    private function defineFiles () {
        $files = [];

        $files = $_FILES;

        return $files;
    }

    private function defineBody () {
        $body = [];

        if (empty($_POST) === FALSE) {
            foreach ($_POST as $key => $value) {
                $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        } else {
            $content = file_get_contents("php://input");

            if ($this->get_headers('Content-Type') == 'application/json') {
                $body = json_decode($content, true); // return as array and not stdObject
            } else {
                parse_str($content, $body);
            }
        }

        return $body;
    }

    private function defineHeaders () {
        $headers = apache_request_headers();


        if (isset($headers['Content-Type']) === TRUE) {
            $headers['Content-Type'] = explode(';', $headers['Content-Type'])[0];
        }

        return $headers;
    }

    private function defineServer () {
        $server = [];

        if (empty($_SERVER) === FALSE) {
            $server = $this->sanitizedArray(INPUT_SERVER, FILTER_SANITIZE_STRING);
        }

        return $server;
    }

    private function setMethods ($methods) {
        if (empty($methods) === FALSE) {

            foreach ($methods as $method => $allowed) {
                $this->methods_addMethod($method, $allowed);
            }

            return true;
        } else {
            return false;
        }
    }

    private function defineApiCall () {
        $api = '';

        if (isset($_GET['url']) === TRUE) {
            $api = rtrim($_GET['url'], '/');
            unset($_GET['url']);
        } else {
            $api = '/';
        }

        return $api;
    }

    public function __construct (
        $methods = []
    ) {
        /**
         * Initiate variables
         */
        $this->session  = new utils\Session();
        $this->api      = $this->defineApiCall();
        $this->method   = $this->defineMethod();
        $this->headers  = $this->defineHeaders();
        $this->params   = $this->defineParams();
        $this->body     = $this->defineBody();
        $this->files    = $this->defineFiles();
        $this->server   = $this->defineServer();
        $this->errors   = [];


        $this->setMethods($methods);


        /**
         * Check if the request method is allowed
         */
        if ($this->methods_methodNotKnown($this->method) === TRUE) {
            $this->errors[] = 501;  // Not Implemented
        } else if ($this->methods_methodNotAllowed($this->method) === TRUE){
            $this->errors[] = 405;  // Method Not Allowed
        }

        /**
         * Check if the file types are allowed
         */
        if ($this->content_isAllowed($this->files) !== TRUE) {
            $this->errors[] = 415;  // Unsupported Media Type
            header('Accept: ' . $this->content_toString());
        }
    }


    public function allowedMethods () {
        $methods = [];

        foreach ($this->methods_arr as $method => $auth) {
            if ($auth === 1) {
                $methods[] = $method;
            }
        }

        return $methods;
    }

    public function set_param ($key, $value) {
        $this->params[$key] = $value;
    }

    public function get_apiCall ($after = 'api') {

        /**
         * if the first word is not a match, return false
         */
        if (strpos($this->api, $after) !== 0) {
            return false;
        }

        /**
         * Otherwise return the api string
         */
        $uri = strpos($this->api, $after) + strlen($after);
        $uri = substr($this->api, $uri);

        $uri = trim($uri, '/');
        $uri = explode('/', $uri);

        return $uri;
    }

    public function get_method  () {
        return $this->method;
    }

    public function get_params  () {
        return $this->params;
    }

    public function get_param ($key = '') {
        if ($key === '') {
            return false;
        }

        if (isset($this->params[$key]) === FALSE) {
            return false;
        }

        return $this->params[$key];
    }

    public function get_body    ($key = '') {
        if ($key !== '') {
            if (isset($this->body[$key])) {
                return $this->body[$key];
            } else {
                return null;
            }
        }

        return $this->body;
    }

    public function get_headers ($key = '') {
        if ($key !== '') {
            if (isset($this->headers[$key])) {
                return $this->headers[$key];
            } else {
                return null;
            }
        }

        return $this->headers;
    }

    public function get_files   () {
        return $this->files;
    }

    public function get_server  () {
        return $this->server;
    }

    public function get_errors  () {
        return $this->errors;
    }

    public function get_first_error  () {
        return $this->errors[0];
    }

    public function errors () {
        if (empty($this->errors) === FALSE) {
            return true;
        } else {
            return false;
        }
    }
}