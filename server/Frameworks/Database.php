<?php


/**
 * Frameworks \ database connection
 *
 * @author  ANDERS OEN FYLLING, anders@nordic.email
 */
namespace Frameworks;
use \PDO;

class Database {

    protected $db;

    function __construct () {

        /**
         * establish a new PDO connection
         */
        $this->db = new PDO(
            'mysql:host=' . c_database_host, c_database_user, c_database_pass,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
        );
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        /**
         * If the database does not exist, it is created.
         */
        $name = "`".str_replace("`","``", c_database_name)."`";
        $this->db->query('CREATE DATABASE IF NOT EXISTS ' . $name);
        $this->db->query('use ' . $name);
        unset($name);

        /**
         *  Now check if the tables exists, if not they are created.
         */
        $exists = $this->db->query('SHOW TABLES');
        if ($exists->rowCount() == 0) {
            //create content
            $this->db->exec( file_get_contents(c_database_file) );
        }
    }
}