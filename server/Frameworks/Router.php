<?php


/**
 * Router \ API class
 *
 * @author  ANDERS OEN FYLLING, anders@nordic.email
 */

namespace Frameworks;

class Router {

    public  $request;
    public  $response;

    private $api;
    private $spa;
    private $controller;
    private $state;

    private $debug = false;

    public function __construct (
        $prefix     = 'api',
        $spa        = 'SPA.php',
        $methods    = [],
        $session    = FALSE,
        $csrf       = FALSE
    ) {
        /**
         * Initiate variables
         */
        $this->request      = new http\Request($methods);
        $this->response     = new http\Response();
        $this->api          = $this->request->get_apiCall($prefix);
        $this->spa          = $spa;
        $this->controller   = '';   //defined in state() if true
        $this->state        = '';

        /**
         * Checks if there were any errors with the request
         */
        if ($this->request->errors() === TRUE) {
            $this->response->failure($this->request->get_first_error());
        }

        /**
         * This determine if its an api call or not.
         *  If it is an api call let the script continue
         *  Otherwise, require the SPA file, set headers and exit the script.
         */
        if ($this->api === FALSE) {
            $this->response->html($this->spa);
        }

    }

    public function state ($state, $controller = '', $callback = '') {

        /**
         * Check that the api state matches the state uri. The match must also
         *  happen at the first character!
         */
        $state = trim($state, '/');
        if (is_array($this->api) === FALSE || $this->api[0] !== $state) {
            return;
        }

        /**
         * update the current api state, by removing the matched subset.
         *  url /users/tests, matches state: /users
         *  api is then updated to /tests.
         *      if (/users/tests === /users/tests) /users/tests => /tests
         *
         * Remeber this is an aray, so we only need to remove the first element.
         */
        $this->state = array_shift($this->api);

        /**
         * Create scope for each http method function in this class
         *  This is done by including this with the arguments
         */
        $params = [];
        $self = &$this;
        foreach ($this->request->allowedMethods() as $method) {
            $params[] = [$self, $method];
        }

        /**
         * This method can either be called as:
         *  func('/state', 'SomeController', function (...) {});
         *  or
         *  func('/state', function (...) {});
         */
        if (is_callable($controller) === TRUE) {
            call_user_func_array($controller, $params);
        } else if (is_callable($callback) === TRUE) {
            $this->controller = $controller;
            call_user_func_array($callback, $params);
        }

        // reset it after the callback
        array_unshift($this->api, $this->state);
        $this->state = '';
    }

    public function method ($method, $uri, $action) {
        //match methods
        if ($method !== $this->request->get_method()) {
            return;
        }

        // convert uri into an array
        $uri = trim($uri, '/');
        $uri = strlen($uri) >= 1 ? explode('/', $uri) : [];

        //first match the array sizes of the uri
        if (sizeof($uri) !== sizeof($this->api) + sizeof($this->request->get_params())) {
            return;
        }


        //then match each element
        $i = -1;
        $len = sizeof($this->api);
        while (++$i < $len) {
            $u = $uri[$i];
            $a = $this->api[$i];


            // if its a match continue
            if ($u === $a) {
                continue;
            }

            // if its not a match, and the a does not start with '?', return
            if ($u[0] !== '?') {
                return;
            }

            // at this stage it means that the a is an wildcard
            //  regex matching
            $u = ltrim($u, '?');
            $u = trim($u, ':');
            $u = explode(':', $u);
            $wildcard = '';


            if (sizeof($u) === 1) {
                //regex type is not set!
                $this->request->set_param($u[0], $a);
            } else if (preg_match('/'.$u[0].'+/', $a) === 1) {
                //do regex check here
                $this->request->set_param($u[1], $a);
            } else {
                //no match
                return;
            }
        }

        //its a match!

        //make sure action is valid
        if (is_string($action) === TRUE || is_callable($action) === TRUE) {
            $arguments = [
                $this->request,
                $this->response
            ];

            if (is_string($action) === TRUE) {
                $class  = explode('.', trim($action));  // controller.method => ['controller', 'method']
                $ctrl   = isset($class[1]) === TRUE ? $class[0] : $this->controller; //if class contains ctrl and method
                $meth = isset($class[1]) === TRUE ? $class[1] : $class[0]; //if class contains controller and method

                if ($ctrl === '') {
                    die('Controller was not specified! TERMINATED SCRIPT!');
                }

                $file = 'server/controllers/' . $ctrl . '.php';
                if (file_exists($file) === TRUE) {
                    require $file;
                    $ctrl = new $ctrl;
                    $ctrl->{$meth}($arguments[0], $arguments[1]);
                } else {
                    die('Controller file does not exist. TERMINATED SCRIPT!');
                }

            } else {
                call_user_func_array($action, $arguments);
            }
        }
    }

    public function get ($uri, $action) {
        $this->method('GET', $uri, $action);
    }

    public function head ($uri, $action) {
        $this->method('HEAD', $uri, $action);
    }

    public function post ($uri, $action) {
        $this->method('POST', $uri, $action);
    }

    public function put ($uri, $action) {
        $this->method('PUT', $uri, $action);
    }

    public function delete ($uri, $action) {
        $this->method('DELETE', $uri, $action);
    }


    public function __call($method, $args) {
        $this->method(strtoupper($method), $args[0], $args[1]);
    }

    public function garbageCollector() {
        $this->response->failure(404);
    }
}