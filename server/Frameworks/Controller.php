<?php

namespace Frameworks;

class Controller {
    protected $model        = null;
    protected $requestCSRF  = false;

    function __construct ($name = null) {
        $this->model = $this->loadModel($name);
    }

    /**
     * Request that a valid csrf token must be present for this controller
     */
    protected function csrf () {
        //TODO: create a config for csrf?
        if (API_S_CSRF === TRUE && Session::get('valid_csrf') !== TRUE) {
            return false;
        }

        return true;
    }

    /**
     * Loads a model into the class. null is returned if the model defined does not exists
     *
     * @param   string              $name   Model class name
     * @return  null|modelObject
     */
    protected function loadModel($name = null) {
        if ($name !== null) {
            $file = c_models_path . $name . '.php';
            if (file_exists($file)) {
                require $file;
                return new $name();
            }
        }

        return null; //not found
    }
}