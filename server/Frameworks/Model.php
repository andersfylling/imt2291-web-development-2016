<?php

namespace Frameworks;
use \PDO;

class Model extends Database{
    protected $dbSchema = '';

    function __construct ($schema) {
        parent::__construct();
        $this->dbSchema = $schema;
    }


    /**
     *
     * ALL:
     * [ ]
     *
     * ONE eg. (aim for unique keys such as id):
     * [ "key" => "value" ]
     *
     * @param $query
     * @param $callback
     */
    public function find ($query = [], $callback) {

        $res = [];
        $code = null;
        $where = '';
        $select = '*';

        if (is_string($query) === TRUE) {
            $where .= ' ' . $query;
        }

        //Generate MySQL query based in $query
        if (empty($query) === FALSE) {
            $where = ' WHERE ';
            foreach($query as $key => $value) {
                //look for _select options
                if (strtolower($key) === '_select') {
                    $select = $value;
                    unset($query[$key]);
                    continue;
                }

                //simple AND
                $end = '';
                if (sizeof($query) > 1 && $value != end($query)) {
                    $end = ' AND ';
                }
                $where .= $key . '=:' . $key . $end;

                unset($query[$key]);
                $query[':' . $key] = $value;
            }
        }

        $stmt = $this->db->prepare('SELECT ' . $select . ' FROM ' . $this->dbSchema . $where);
        $stmt->execute($query);

        if ($stmt->rowCount() > 0) {
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $code = 404;
        }

        $this->callback($callback, $res, $code);
    }

    /**
     * Used to update an existing instance.
     *
     * @param $query
     * @param $callback
     */
    /*
    protected function update ($query, $callback) {
    }
    */


    /**
     * Used to create a new instance. returns code 201 created if everything went ok
     *
     * @param $query
     * @param $callback
     */
    public function insert ($query, $callback) {
        $res    = [];
        $code   = null;

        //get column names
        $stmt = $this->db->prepare('DESCRIBE ' . $this->dbSchema);
        $stmt->execute();
        $keys = $stmt->fetchAll(PDO::FETCH_COLUMN);

        //create a query statement, and set param values if exists other NULL
        $queryStart = 'INSERT INTO ' . $this->dbSchema . '(';
        $queryEnd = ' VALUES(';
        $params = [];
        foreach ($keys as $key) {

            if (in_array($key, ['id', 'created', 'updated'])) {
                continue;
            }

            $queryStart .= $key;
            $queryEnd   .= ':' . $key;

            if (isset($query[$key]) === TRUE) {
                if ($key == 'password') {
                    $params[':' . $key] = password_hash($query[$key], PASSWORD_DEFAULT);
                } else {
                    $params[':' . $key] = $query[$key];
                }
            } else {
                $params[':' . $key] = NULL;
            }

            $queryStart .= ',';
            $queryEnd   .= ',';
        }

        $statement = rtrim($queryStart, ',') . ')' . rtrim($queryEnd, ',') . ')';

        //database action
        $stmt = $this->db->prepare($statement);
        $stmt->execute($params);

        if ($stmt->rowCount() > 0) {
            $res = [
                'id' => $this->db->lastInsertId()
            ];
            $code = 201;
        } else {
            $res = [
                'msg' => 'Could not be created.'
            ];
            $code = 500;
        }

        $this->callback($callback, $res, $code);
    }

    /**
     * Deletes an entry using its id.
     *
     * @param float|int|string  $query
     * @param function          $callback
     */
    protected function remove ($id, $callback) {
        $res    = [];
        $code   = null;

        $stmt = $this->db->prepare('DELETE FROM ' . $this->dbSchema . ' WHERE id=:id');
        $stmt->execute([
            ':id' => $id
        ]);

        $this->callback($callback, $res, $code);
    }

    /**
     * Checks that the callback method is valid and calls it
     *
     * @param $response
     * @param $callback
     */
    protected function callback ($callback, $res, $code = 200) {
        if (is_callable($callback) === TRUE) {
            call_user_func_array($callback, [$res, $code]);
        }
    }
}